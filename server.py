
from flask import Flask, Response, request
import tempfile
import argparse
import copy

from dataclasses import dataclass
from multiprocessing import cpu_count
from pathlib import Path
import os

import numpy as np
import torch
import torchaudio
from torch.utils.data import DataLoader
from tqdm import tqdm
import math

from constants import HIDDEN_SIZE, TARGET_SAMPLE_RATE
from data import FixedSegmentationDatasetNoTarget, segm_collate_fn
from eval import infer
from models import SegmentationFrameClassifer, prepare_wav2vec

from supervised_hybrid.segment import pdac, update_yaml_content

import threading

app = Flask(__name__)

lock = threading.Lock()

def load_model(lang, args):
    print(" ---- Loading model %s ----"%lang)

    device = (
        torch.device(f"cuda:0")
        if torch.cuda.device_count() > 0
        else torch.device("cpu")
    )
    print("Using device",device)

    f = args.model_dir+"/"+lang+".pt"
    if not os.path.isfile(f):
        print(" ---- Model does not exits, using multilingal backup ----")
        return None
    checkpoint = torch.load(f, map_location=device)

    # init wav2vec 2.0
    wav2vec_model = prepare_wav2vec(
        checkpoint["args"].model_name,
        checkpoint["args"].wav2vec_keep_layers,
        device,
    )
    # init segmentation frame classifier
    sfc_model = SegmentationFrameClassifer(
        d_model=HIDDEN_SIZE,
        n_transformer_layers=checkpoint["args"].classifier_n_transformer_layers,
    ).to(device)
    sfc_model.load_state_dict(checkpoint["state_dict"])
    sfc_model.eval()

    print(" ---- Model loaded ----")

    return wav2vec_model, sfc_model, device

def segment_shas(args, model, max_segment_length, min_segment_length):
    wav2vec_model, sfc_model, device = model

    yaml_content = []
    for wav_path in tqdm(sorted(list(Path(args.path_to_wavs).glob("*.wav")))):

        # initialize a dataset for the fixed segmentation
        dataset = FixedSegmentationDatasetNoTarget(
            wav_path, args.inference_segment_length, args.inference_times
        )
        sgm_frame_probs = None

        for inference_iteration in range(args.inference_times):

            # create a dataloader for this fixed-length segmentation of the wav file
            dataset.fixed_length_segmentation(inference_iteration)
            dataloader = DataLoader(
                dataset,
                batch_size=args.inference_batch_size,
                num_workers=min(cpu_count() // 2, 4),
                shuffle=False,
                drop_last=False,
                collate_fn=segm_collate_fn,
            )

            # get frame segmentation frame probabilities in the output space
            probs, _ = infer(
                wav2vec_model,
                sfc_model,
                dataloader,
                device,
            )
            if sgm_frame_probs is None:
                sgm_frame_probs = probs.copy()
            else:
                sgm_frame_probs += probs

        sgm_frame_probs /= args.inference_times

        segments = pdac(
            sgm_frame_probs,
            max_segment_length,
            min_segment_length,
            args.dac_threshold,
        )

        yaml_content = update_yaml_content(yaml_content, segments, wav_path.name)

    print(
        f"Created SHAS segmentation with max={max_segment_length} & "
        f"min={min_segment_length}"
    )

    return yaml_content


def pcm_s16le_to_tensor(pcm_s16le):
    audio_tensor = np.frombuffer(pcm_s16le, dtype=np.int16)
    audio_tensor = torch.from_numpy(audio_tensor)
    audio_tensor = audio_tensor.float() / math.pow(2, 15)
    audio_tensor = audio_tensor.unsqueeze(1) # shape: frames x 1 (1 channel)
    return audio_tensor

@app.route('/segmenter/<lang>/infer', methods=['POST'])
def segment(lang):
    global mult_model

    if not lang in models:
        ret = load_model(lang,args)
        if ret is None:
            if mult_model is None:
                ret = load_model("mult",args)
                if ret is None:
                    raise Exception
                mult_model = ret
            else:
                ret = mult_model
        models[lang] = ret

    if "audio" not in request.files:
        return "No audio file recieved", 400

    with tempfile.TemporaryDirectory() as name:
        pcm_s16le = request.files["audio"].read()
        src = pcm_s16le_to_tensor(pcm_s16le)

        torchaudio.save(name+"/audio.wav",src,16000,channels_first=False,format="wav",encoding="PCM_S")

        args_tmp = copy.deepcopy(args)
        args_tmp.path_to_wavs = name

        max_segment_length = float(request.files["max_segment_length"].read()) if "max_segment_length" in request.files else args.dac_max_segment_length
        min_segment_length = float(request.files["min_segment_length"].read()) if "min_segment_length" in request.files else args.dac_min_segment_length

        with lock:
            yaml_context = segment_shas(args_tmp, models[lang], max_segment_length, min_segment_length)

        out = [(segment["offset"],segment["offset"]+segment["duration"]) for segment in yaml_context]
        return out

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--inference_batch_size",
        "-bs",
        type=int,
        default=12,
        help="batch size (in examples) of inference with the audio-frame-classifier",
    )
    parser.add_argument(
        "--inference_segment_length",
        "-len",
        type=int,
        default=20,
        help="segment length (in seconds) of fixed-length segmentation during inference"
        "with audio-frame-classifier",
    )
    parser.add_argument(
        "--inference_times",
        "-n",
        type=int,
        default=1,
        help="how many times to apply inference on different fixed-length segmentations"
        "of each wav",
    )
    parser.add_argument(
        "--dac_max_segment_length",
        "-max",
        type=float,
        default=18,
        help="the segmentation algorithm splits until all segments are below this value"
    )
    parser.add_argument(
        "--dac_min_segment_length",
        "-min",
        type=float,
        default=0.2,
        help="a split by the algorithm is carried out only if the resulting two segments"
        "are above this value (in seconds)",
    )
    parser.add_argument(
        "--dac_threshold",
        "-thr",
        type=float,
        default=0.5,
        help="after each split by the algorithm, the resulting segments are trimmed to"
        "the first and last points that corresponds to a probability above this value",
    )
    parser.add_argument(
        "--model_dir",
        type=str,
        default="models",
    )
    args = parser.parse_args()
    return args

if __name__ == "__main__":
    args = get_args()
    models = {}
    mult_model = None

    app.run(debug=True, host="0.0.0.0", use_reloader=True, port=5001)
